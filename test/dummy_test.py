import inspect
import logging

import requests

logger = logging.getLogger("Test")
logger.setLevel(logging.DEBUG)


def test_pass():
    pass


def test_print():
    print(f"Sample print statement from {inspect.stack()[0][3]} method")


def test_logger_debug():
    logger.debug(f"Sample logger debug statement from {inspect.stack()[0][3]} method")


def test_logger_info():
    logger.info(f"Sample logger info statement from {inspect.stack()[0][3]} method")


def test_logger_error():
    logger.error(f"Sample logger error statement from {inspect.stack()[0][3]} method")


def test_logger_critical():
    logger.critical(f"Sample logger critical statement from {inspect.stack()[0][3]} method")


def test_logger_requests():
    response = requests.get("https://httpbin.org/get")
    logger.info(f"Status code from get request {response.status_code} inside method {inspect.stack()[0][3]}")