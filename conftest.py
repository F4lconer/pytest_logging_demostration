import logging

import pytest
from _pytest.fixtures import FixtureRequest

logger = logging.getLogger("Test")
logger.setLevel(logging.DEBUG)


@pytest.fixture(autouse=True, scope='function')
def logging_fixture(request: FixtureRequest):
    logger.info(f"Start of a test {request.node.name}")
    yield
    logger.info(f"End of a test {request.node.name}")
